let checkAuthendication = () => {
  const credentials = localStorage.getItem("mobileCredentials");
  const isInLoginPage = window.location.pathname === "/views/login.html";

  if (!credentials && !isInLoginPage) {
    window.location.assign("login.html");
  }
  if (credentials && isInLoginPage) {
    window.location.assign("admin.html");
  }
};
checkAuthendication();
